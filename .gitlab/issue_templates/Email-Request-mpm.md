### Goal

`TYPE OF ONE-TIME EMAIL: Choose from below, delete the others`
1.  Security notice @nbsmith
2.  Support notifications @nbsmith
3.  Others (please elaborate) @nbsmith

| **Question** | **Example** | **Requester to fill in** |
| -------- | -------- | -------- |
| What is the goal of this email?   | Invite to a future event in recipient's city   | `XXX`   |
| What is the main CTA (call-to-action) of this email?   | Event XYZ registration   | `XXX`   |
| Send test email to (by default all email tests are sent to content@):   | ownerofevent@gitlab   | `XXX`   |

>  *For the GOAL of the email, please be specific. "To follow up" does not describe what we hope to gain from following up with them. Inviting them to a future event tells us that the goal is to garner registrations for the upcoming event (which ties to SCLAU).*

---
### Overview

* *Send Date:* `month, date, year`
* *Recipients:* `list upload from event, registered but no-showed, etc.`
* *From Name:* `GitLab Team, etc.`
* *From Email:* `info@gitlab.com, etc.`

---
### Content

*Subject Line:* `place here`

*Email Body:*

`place email body here`

---
### Special Notes

*Include anything else you need the Marketing Program Manager to know about the send.*

---
### Next Steps

Marketing Program Manager will: 
1.  ask any questions in the comment of this issue
2.  create the email in Marketo
3.  make any suggested edits to copy and communicate in the comments of this issue
4.  send the test email to the person requesting the email and designated additional recipients 
5.  set the email to send on agreed upon date
6.  confirm the send in the comments of this issue

Questions refer to [handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/index.html#requesting-an-email)

fyi @jjcordz

/assign @nbsmith

/label ~Events ~"MktgOps-Priority::2 - Action Needed" ~MktgOps ~"Marketing Campaign" ~"Marketing Programs" ~"mktg-status::plan"

