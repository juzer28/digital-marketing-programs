## Purpose
To plan for and expire analyst assets as they near the end of their reprint rights. Add relevant DRIs for each of the action items below.

URL: 

Original Gating Issue: 

`Set the due date as the date of expiration` - add a notice with this issue link to the calendar with all DRIs included below.

## Action Items
* [ ] Redirect page ()
* [ ] Remove from PathFactory (@sdaily)
* [ ] Remove from nurture where used ()
* [ ] Remove references from website ()
* [ ] Change CTA if used in blogs ()
* [ ] Remove where used in ads ()
* [ ] Notify sales not to use it if they are using it ()
* [ ] Remove visible googledrive link - so that people can't reference it ()

cc @tompsett + MPM @jgragnola @zbadgley @aoetama @jennyt @nbsmith @Mmmitchell @eirinipan + DMP @sdaily @mnguyen4 @shanerice

/confidential

/label ~"mktg-status::wip" ~"Marketing Programs" ~"Digital Marketing Programs" ~"Analyst Relations" ~"PathFactory" ~"Asset Expiration"