# GATED CONTENT DETAILS

#### Execution Timeline

📦 **Asset Delivery Date: `X`** - must be provided by PMM owner (date the temporary analyst asset will be provided to GitLab for preparation)  
🚀 **Asset Launch Date: `X`** - must be provided by PMM owner (for analyst reports, date they release the report)  
📅 **Pushed Live Date: `X`** - MPM to fill in when page is live  
✅ **Aligned Campaign: `X`** - choose from active campaigns listed [here](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1839132097). All content should align to live and upcoming campaigns.  

#### DRIs & Analyst Links
* **PMM Owner:** 
* **MPM Owner:** 
* **Content Owner:**
* **Official Content Name:**  `this should be exactly as it appears from the analyst`
* **Link to Content:** (link from analyst - include temporary unlicensed doc if available)
* **Validity / Expiration Date:** (start date - end date)
* **Marketo:** 
* **Salesforce:** 
* **Reprint Rights:** [example - Forrester Reprint Rights](https://www.forrester.com/staticassets/marketing/about/Forrester_Reprint-Client-Guide.pdf)
* **Citation Policy:** [example - Forrester Citations Policy](https://www.forrester.com/marketing/policies/citations-policy.html)

### [Landing Page Copy Document >>](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#) - *make a copy of this template and provide requested copy*

# ACTION ITEMS

## Create landing page
* [ ]  Write copy for landing page - PMM owner
* [ ]  Provide edits to copy in Copy Document - MPM
* [ ]  Create page on www-gitlab-com [quick link to repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources) - MPM
* **While pipeline running, skip to steps below**
* [ ]  Test pipeline-approved page - MPM

## Create program in Marketo
* [ ]  Clone gated content template Marketo program - MPM ([mkto link](https://app-ab13.marketo.com/#PG2524A1)) - use naming [year]_[type]_[name]
* [ ]  Edit flows for smart campaign - MPM
* [ ]  Update tokens with 3 bullets of landing page and Pathfactory link - MPM

## Create campaign in Salesforce
* [ ]  In Marketo program, under "salesforce campaign sync" choose "create new" and include link to epic
* [ ]  Find the campaign in SFDC and update the start date to be the launch date and end date 1 year later - MPM ([sfdc link](https://gitlab.my.salesforce.com/701?fcf=00B61000004NYQP&rolodexIndex=-1&page=1))
* [ ]  Update SFDC campaign owner to be MPM - MPM

## Analyst review
* [ ]  **Provide approved page in review app to PMM - MPM**
* [ ]  **Request review from analyst** - PMM
* [ ]  COMMENT IN ISSUE WITH SCREENSHOT OF ANALYST APPROVAL AND @ MPM - requester

## Test and push live
* [ ]  Merge MR - MPM
* [ ]  Test fully functioning page live - MPM
* [ ]  Notify requester in issue comments that page is live and traffic can be pushed there - MPM
* [ ]  Update [#content-updates Slack channel](https://gitlab.slack.com/messages/C90CPFE2W) with gated link and Pathfactory link - MPM

## Add to Pathfactory
* [ ]  Comment to @sdaily the Pathfactory target track to add the asset to (AND location within track to add to) - MPM
* [ ]  Upload PDF or video to Pathfactory - [follow uploading instructions in handbook]() - @sdaily
* [ ]  Add asset to campaign track - @sdaily
* [ ]  Provide link to asset within Pathfactory Track in comments and tag MPM - @sdaily
* [ ]  Update tokens in Marketo with Pathfactory link (do not include email tracking link) - MPM
* [ ]  Update url in WIP landing page MR with Pathfactory link (at url closing, include `&lb_email=` *without* marketo email token) - MPM

## Set up listening campaign in Marketo
* [ ]  Ping MktgOps using ~MktgOps label to set up a listening campaign in Marketo for this asset

Questions refer to [handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/)

/assign @jgragnola 

/confidential

/label ~"Marketing Campaign" ~"Marketing Programs" ~"MktgOps - FYI" ~"MPM - Landing Page & Design" ~"mktg-status::wip"