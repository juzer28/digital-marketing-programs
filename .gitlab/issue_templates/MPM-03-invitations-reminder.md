## 📸  Final Email
**Sender: GitLab, info@**  
**Subject: fill in when final**

*copy paste image of most recent email here - make sure to update when new versions are approved*

## Purpose

This issue will track the invitation and reminder emails for events. Adherence to schedules by supporting teams (field marketing and corporate marketing) are required to hit deadlines for promotion in advance of event.

**Below to be completed by Marketing Program Manager with support of Field Marketing and Corporate Marketing.**

**The first email invitation will take place 30 business days prior to the event start date. Invitation two will be sent 15 business days prior to the event start date, and an optional third email may be sent 5 business days prior to the event. The reminder email will be sent the day prior to the event (not business day).**

This event requires:

* [ ] **SALES-NOMINATED INVITE:** `x` (Write copy by: `x`) (optional)
* [ ] **PLANNED INVITE 1 DATE:** `x` (Write copy by: `x`)  
* [ ] **PLANNED INVITE 2 DATE:** `X` (Write copy by: `x`)  
* [ ] **PLANNED INVITE 3 DATE:** `X` (Write copy by: `x`)  (optional)  
* [ ] **PLANNED REMINDER DATE:** `X` (Write copy by: `x`)  

## 🗓 Add invitation emails to [email marketing calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&ctz=America%2FLos_Angeles)
* [ ]  Invitation email 1
* [ ]  Invitation email 2
* [ ]  Invitation email 3 (optional)
* [ ]  Reminder email

## 📧 Create Invitation 1 Email

* [ ]  Write copy for email - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test and forward email - MPM
* [ ]  Approve test email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 📧 Create Invitation 2 Email

* [ ]  Write copy for email - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test and forward email - MPM
* [ ]  Approve test email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 📧 Create Invitation 3 Email

* [ ]  Write copy for email - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test and forward email - MPM
* [ ]  Approve test email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 🔔 Create Reminder Email

* [ ]  Write copy for email - MPM (campaign owner to clarify ahead of time if agenda is required for the reminder)
* [ ]  Set up email in Marketo - MPM
* [ ]  Test and forward email - MPM
* [ ]  Approve test email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

## 📧 Create Sales-Nominated Email (delete this section if not required)

* [ ]  Duplicate invitation email in Marketo and edit copy as needed - MPM
* [ ]  If no invitation email is available, write copy for email - campaign owner
* [ ]  Set up sales-nominated flow in Marketo
* [ ]  Test and forward email - MPM
* [ ]  Approve test email - campaign owner
* [ ]  Confirm in issue comments that email is ready for sales to use - MPM

Please submit any questions about this template to the [#marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MPM - Invitations & Reminder"