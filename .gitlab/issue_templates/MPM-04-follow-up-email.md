## 📸  Final Email
**Sender: GitLab, info@**  
**Subject: fill in when final**

*copy paste image of most recent email here - make sure to update when new versions are approved*

## Purpose

This issue will track the steps to execute a follow up email for an event, webcast, or other campaign.

**Below to be completed by Marketing Program Manager with support of Field Marketing or Corporate Marketing.**

**PLANNED SEND DATE:** `X` (Write copy by: `X`)

## 🗓 Add follow up email to [email marketing calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&ctz=America%2FLos_Angeles)
* [ ]  Add follow up email to email marketing calendar

## 📧 Create and test email in Marketo
* [ ]  Write copy for email(s) - specify for attendees/no shows - campaign owner
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo and edit copy as needed - MPM
* [ ]  Test email and send to owner for review - MPM
* [ ]  Test and approve email - campaign owner
* [ ]  Confirm in issue comments that email is set to send - MPM
* [ ]  Confirm in issue comments that email was sent out - MPM

Please submit any questions about this template to the [#marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MPM - Follow Up Emails"