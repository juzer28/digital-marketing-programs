## Purpose

This issue will track post event leads that are requested to be added to [TOFU nurture tracks](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#top-funnel-nurture).

**To be completed by Marketing Program Manager:**

  * [ ] Modify the request campaign flow step to reference the correct stream as indicated by FMM in the Epic
  * [ ] Run once the marketo smart campaign
  * [ ] Confirm leads have been added in the issue comment tagging the FMM
 
Please submit any questions about this template to the [#marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MktgOps - FYI" ~"MPM - Add to Nurture"