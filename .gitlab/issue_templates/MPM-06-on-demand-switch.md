## Purpose

This issue will track the process of converting a LIVE webcast to an on-demand version.

**To be completed by Marketing Program Manager:**

## :video\_camera: Youtube & Landing page

* [ ] Upload recording to YouTube - MPM (Same day post webcast)
* [ ] Add Youtube url to landing page in www-gitlab-com - MPM (1 BD post webcast)

/assign @aoetama

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MPM - Switch to On-Demand"