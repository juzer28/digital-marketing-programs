### 📝 [Epic Link >>](#)

## Purpose

This issue is to be filled out when requesting MPM support for virtual events. For virtual events that need to be hosted using our internal webcast platform, MPM will confirm proposed event date does not coincide with other pre-scheduled virtual events.

[>>Link to check pre-scheduled GitLab hosted virtual events](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)

[>>Click to see MPM support tasks for all virtual events](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#virtual-events-process)

# VIRTUAL EVENT DETAILS (`Required to be filled out when requesting a webcast`)

* **Requestor:**
* **Event Type:** (`check the option below that applies`)
* [ ]  End user webcast - GitLab Hosted
* [ ]  End user webcast - Not GitLab Hosted
* [ ]  Virtual Sponsorship
* [ ]  Account specific webcast
* [ ]  Reseller webcast
* [ ]  Community virtual event support
* **Business Goal:** (`select all that applies`)
* [ ] Increase Awareness about GitLab
* [ ] Increase SCLAU/Pipeline/Pipe-to-Close Rate 
* [ ] Educate users/customers 
* [ ] Upsell users/customers 
* [ ] Others - please elaborate :
* **Audience:** (`select all that applies`)
* [ ] Potential Prospects who have never heard about GitLab 
* [ ] Prospects we're currently selling to
* [ ] Non-paying customers
* [ ] Paying customers
* [ ] Authorized resellers
* [ ] Community members
* **Topic:**
* **Event Date:**
* **Speaker:** 
* **Have Paid Ads Budget:** Yes/No
* **Paid Ads Budget amount:**  (`only fill out if have Paid Ads budget`)


## `To be filled out when requesting Account specific webcast only: `
* **SDR** (if one is assigned to account or assisting with outreach):     
* **SA Assigned to Webcast** (please arrange this before making this webcast request): 

/label ~"Marketing Programs" ~"MPM - Radar"  ~"mktg-status::plan"

/assign @aoetama