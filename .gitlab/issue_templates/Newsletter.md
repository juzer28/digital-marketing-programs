# NEWSLETTER DETAILS (`Required to be filled out before requesting a newsletter`)

* *Send Date:* `month, date, year`
* *Recipients:* `Newsletter Opt-Ins, Authorized Resellers, etc.`
* *From Name:* `GitLab Team, etc.`
* *From Email:* `info@gitlab.com, etc.`
* *Subject Line:* `Put Here`

## Submitting newsletter content ideas

When you have newsletter content ideas, please submit them here as a comment using the comment template below (just copy/paste and fill in) by the due date listed in this issue. We will edit and refine copy and title for you if selected for the email, we just need your ideas when you have them. Just pop 'em in here as you run into stuff and think "newsletter worthy!?"

### Comment template

* title/topic:
 * copy:
 * CTA:
 * link:
 * link is live/available as of this comment? YES/NO

* title/topic:
 * copy:
 *  CTA:
 * link:
 * link is live/available as of this comment? YES/NO

* title/topic:
 * copy:
 *  CTA:
 * link:
 * link is live/available as of this comment? YES/NO

* title/topic:
 * copy:
 *  CTA:
 * link:
 * link is live/available as of this comment? YES/NO
 
### Next Steps

* [ ] MPM (@aoetama) will clone/rename this [google doc](https://docs.google.com/document/d/1pmiLz7ZkHEcR4Bj9-mIlQ5GljUTkaEYME9YmkLgpZSo/edit#) and compile newsletter content (`5 business days prior to send`)
* [ ] Editorial team member will add/review/polish content in Google doc (`5 business days prior to send`)
* [ ] MPM (@aoetama) will configure polished content, set up program and tracking in marketo (`2 business days prior to send`)
* [ ] MPM (@aoetama) will send final marketo test email to content team (`1 business day prior to send`)
* [ ] Editorial team member will review test email and share final feedback (if necessary) in this issue (`1 business day prior to send`)

/cc @jgragnola @jjcordz
/assign @aoetama
/label ~"Marketing Campaign" ~"Marketing Programs" ~"MktgOps - FYI" ~Newsletter ~"mktg-status::plan"
<!-- See the handbook for Editorial team member to assign: https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/index.html#process-for-bi-weekly-newsletter -->