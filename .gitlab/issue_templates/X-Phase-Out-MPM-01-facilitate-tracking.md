### 📝 [Epic Link >>](#)

## Purpose

This issue will track the creation of campaign in Marketo/Salesforce and link the two for consistency between systems. Finance tags are requested from AP.

**To be completed by Marketing Program Manager:**

## 🏷 Create finance tag
[Link to list of tag requests >>](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0&fvid=1051154680)
**Tag to use:** `MPM list here`

## 💙 Create Salesforce & Marketo Campaign(s)
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()
  * [ ] [additional salesforce campaign]()
  * [ ] [additional marketo program]()

## 🔗 Link MKTO <> SFDC
  * [ ] main campaign
  * [ ] additional campaign

## :green\_heart: Create Zoom Program(s) (only for GitLab Hosted Webcasts)
  * [ ] [Zoom program](https://zoom.us/webinar/list)

## 🔗 Link MKTO <> Zoom (only for GitLab Hosted Webcasts)
 * [ ]  Update zoom webinar id in Marketo token
 * [ ]  Set up integration in zoom to send registration, attendees, poll , and Q&A information to marketo
 
Please submit any questions about this template to the #marketing-programs slack channel.

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MPM - Facilitate Tracking" ~"MktgOps - FYI"