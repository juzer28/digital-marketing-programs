# Digital Marketing Promotion requests
If you are requesting digital marketing support for a campaign, event, webcast, new whitepaper, paid sponsorship, display ads, syndication, etc, please fill out the details below.  If you don't know all the details, fill in as much as you can.

### Content for display advertising, paid search, and/or paid social support
### We need at least 2 weeks to prepare most display advertising, paid search, and/or paid social campaigns because of ad design and agency turnaround time

#### All information below needs to be completed by requester to launch a digital advertising campaign

* URL to related Epic: `Enter URL of Epic`

* If no Epic, please make a copy and fill out [Campaign Brief](https://docs.google.com/document/d/1Ba6jBhaJp-2TmAhr7HKDzQVGyNLO3D-t7TV4Wk21RlU/edit?usp=sharing): `Enter URL of campaign brief`

* GitLab URL to promote: `Enter URL`

* Purpose for advertising request: `Summary of why you are requesting advertising support`

* Goal you want to achieve with advertising: `Enter tangible goal for this campaign (i.e. registrations, downloads, traffic, etc)`

* Start and End Dates: `Enter start and end date of advertising -- if this content expires, add the expiration date`

* Budget for digital marketing campaign: `Enter in budget for campaign, you may not know this, but if you have spend, add it here`

* Creative Asset: `Enter URL(s) to GitLab creative asset(s) to use or URL to the design request issue`
     * This may require a separate ask for Design team, please make request in Corporate project and factor into timeline
     * Search [creative repository](https://gitlab.com/gitlab-com/marketing/corporate-marketing/tree/master/design) if you want to use existing creative 

#### Display advertising creative requirements:
* 150KB Limit
* File Format (one of the following): .gif, .jpg, .jpeg, .png
* Ensure that copy on banner and cta are easily readable, especially on smaller banners
* Sizes:
     * 728x90
     * 300x250
     * 300x600
     * 160x600
     * 970x250
     * 300x1050
     * 336x280
     * 320x50

#### Paid Social creative requirements:
* Facebook: `1200x628 pixels`
     * No buttons allowed (trick is to have an edge-to-edge bar at the bottom that houses your CTA)
     * Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
     * Text should not take up more than 20% of the image"
* LinkedIn: `1200x627 pixels`
     * Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* Twitter: `1200X675 pixels`
     * Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message

## Targeting
### This information is required for every type of promotion. Fill in as much as you can.

#### Need help if targeting ideas? Review this [list of targeting options](https://docs.google.com/spreadsheets/d/1ZuzFj-R7uPng76hI3XFx0kclR_uFIY5hcg0ikTZk4KM/edit?usp=sharing)

* Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. cities, states, countries, etc.)`

* Industries: `Enter desired industries to target (we will match as closely as possible)`

* Fields of Study (if applicable): `Enter targeted field of study`

* Audience Job Title: `Enter targeted job titles`

* Audience Skills: `Enter skills of people you wish to target`

* Audience Interests: `Enter topics of interest of the targeted audience`

* Salesforce Campaign (if applicable): `Enter URL to Salesforce campaign`

## Any marketing promotion that is not display advertising, paid search, or paid social
### If this promotion is not already managed by DMP, for example sponsorships on external sites, you must answer the following questions
* Who is the target audience? 

* Have you heard of this publication/site/newsletter before they reached out with offer?

* Is this publication/site/newsletter respected in the industry?

* What is the cost?

* How long will it run?

* How many impressions will GitLab get for this? 

* Other than impressions, what other KPIs should we expect?

* How many links will we be able to add? 

* Can we UTM code these links?

* What is the average CTR on links?

* What exactly do they need for this package? 
   * Text? 
   * Creative?  
   * What are the specifications?  
   * How far in advance do you need it?


## Digital Marketing Program to complete**

Paid channels to run campaign

- [ ] Search: 

- [ ] Social: 

- [ ] Demand Gen Sponsorships: 

- [ ] Other (please describe): 

## Digital Marketing team actions

- [ ] Tag events, but first determine what events we need to track based on primary CTA
- [ ] Set-up a/b test [or delete this if there is none]
- [ ] Create UTM codes and share with team


If gated assets are needed, please link to these requests  as well as any other related issues and epics.

/assign @mnguyen4 

/label ~"Digital Marketing Programs" ~"mktg-status::plan"

