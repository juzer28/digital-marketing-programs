* **Subject:**
* **CTA: [[type] title](pathfactory link)**

[add email screenshot]

`pathfactorylink&lb_email={{lead.Email Address}}` 

## Execution Progress

### [Email Copy](link to copy googledoc)

### Action Items
* [ ]  Write copy for email - tag DRI
* [ ]  Provide edits in Copy Document - tag DRI
* [ ]  Set up email in Marketo nurture - tag DRI 
* [ ]  Test email and make any edits - tag DRI
* [ ]  Test email final - tag DRI
* [ ]  Add to nurture stream - tag DRI
* [ ]  Activate content in stream - tag DRI
* [ ]  Test in nurture stream - tag DRI

/confidential

/label ~"Marketing Programs" ~"Email Nurture" ~"mktg-status::wip" 