# Pathfactory promotion requests
Use this form to request any PathFactory related actions.  Choose what you want to happen below.


## Add new URL to existing Target Track 
Choose one or more: 
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* New URL: `URL`
## Add new URL to existing Recommended Track 
Choose one or more: 
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* New URL: `URL`
## Add a Website Promoter to a page
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* URL of page to add promoter to: `URL`

## Create a new Target or Recommended track
Describe in detail the goal of track, audience, proposed content, and why it does not fit in any existing tracks.

## Direct an existing form to a PathFactory path
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* URL where form lives: `URL`

## Create a new form to direct into PathFactory
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* URL where form lives: `URL`

## Create a new form to direct into PathFactory
* [Content pillar](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#current-pillars):
* [Integrated campaign](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#tracking-content/):
* [Topic](https://about.gitlab.com/handbook/marketing/campaigns/):
Required:
* Link to email request form: `URL`

## For Pathbuilder
- [ ] Add to track 
- [ ] Create new track if needed
- [ ] Determine form strategy
- [ ] Enable and test
- [ ] Assign to MPM if new email or form redirection is needed


/assign @sdaily
/cc @nlarue

/label ~PathFactory ~"mktg-status::plan"
