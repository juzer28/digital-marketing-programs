## Set up a new redirect


* Old URL to redirect: `Old URL goes here`
* New URL: `New URL goes here`


/assign @shanerice

/label ~"Digital Marketing Programs" ~"mktg-status::wip" ~"SEO"